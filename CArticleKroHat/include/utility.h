#include <iostream>
#include <sstream>
#pragma once

template<typename T>
class utility
{
    public:
        utility();
        ~utility();
    protected:
    private:

        static string toString(const T& value);
};

