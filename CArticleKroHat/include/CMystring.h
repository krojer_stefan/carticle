#pragma once
#include <iostream>

using namespace std;
class CMyString
{
    public:
        CMyString(char *cStr="");
        CMyString(CMyString *cMstr);
        ~CMyString();
        int getLength();
        void show();
        void append(const CMyString &str);
        void addChar(char ch);
        char getCharAt(int idx);
        int compare(const CMyString &str);
        void toUpper();
        void toLower();
        friend ostream& operator<<(ostream& o,const CMyString);
    protected:
    private:
        char *str;
};
