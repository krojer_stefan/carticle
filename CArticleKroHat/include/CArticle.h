#include "CDate.h"
#include <iostream>
#include <string>
#pragma once

using namespace std;
class CArticle
{
    public:
        CArticle(int id=0, int Typ=HARDWARE, char *Text=" ", int d=1, int m=1, int y=2000, double price =0);
        CArticle(int id, int Typ, string &name, CDate &Date, double price) ;
        CArticle(int id);
        ~CArticle();
        void setPrice(double price);
        double getPrice(){return price;}
        static int getInst();
        static const int HARDWARE=0;
        static const int SOFTWARE=1;
        bool operator<(const CArticle &y);
        bool operator>(const CArticle &y);
        bool operator==(const CArticle &y);
        void operator++();
        void operator++(int differ);
        void operator--();
        void operator--(int differ);
        friend ostream& operator<<(ostream& o,const CArticle);
    protected:
    private:
        const int id;
        int Typ;
        std::string Label;
        CDate inStockSince;
        double price;

        static  int INSTANCE;

};
