#pragma once
#include <iostream>

using namespace std;

class CDate
{
    public:
        CDate(int day = 1,int month = 1,int year = 2000);
        CDate(const CDate &Date);
         int Getday() { return m_day; }
        void Setday( int val) { m_day = val; }
         int Getmonth() { return m_month; }
        void Setmonth( int val) { m_month = val; }
         int Getyear() { return m_year; }
        void Setyear( int val) { m_year = val; }
        void setDate(int day, int month, int year );
        bool isLeapYear(int year);
        void show(int type);
        void add(int day);
        int dif(int day);
            static const int SHORT=0;
         static const int MEDIUM=1;
         static const int   FULL=2;
         friend ostream& operator<<(ostream& o,const CDate);
        const string toString();
    protected:
    private:
         int m_day;
         int m_month;
         int m_year;

};
