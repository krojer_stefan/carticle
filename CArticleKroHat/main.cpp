#include <iostream>
#include "CDate.h"
#include "CArticle.h"
using namespace std;


int main() {
    //cout << "Current article instance count: " << CArticle::getInst();
    CArticle a1(1001, CArticle::HARDWARE, "Optical Mouse",
                10, 8, 2012, 12.90);
    CArticle* aPtr = new CArticle[10];  //!!Default Constructor used
    cout << a1<< endl;
    cout << "Current article instance count: " << CArticle::getInst()<<endl;
    delete[] aPtr;
    cout << "Current article instance count: " << CArticle::getInst()<<endl;
}

