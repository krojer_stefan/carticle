#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <iomanip>
#include "CDate.h"
#include "utility.h"
#include <sstream>
using namespace std;
CDate::CDate(int day,int month,int year) : m_day(day),m_month(month), m_year(year)
{
}
CDate::CDate(const CDate &Date): m_day(Date.m_day),m_month(Date.m_month),m_year(Date.m_year)
{
}


void CDate::setDate(int day, int month, int year ){
    if(day>31||month>12){
        day= 1;
        month = 1;
        year = 2000;
    }
    CDate::Setday(day);
    CDate::Setmonth(month);
    CDate::Setyear(year);
}

bool CDate::isLeapYear(int year){
    if(year % 4 == 0  && year % 100 != 0 || year  % 400 == 0){
        printf("Das Jahr ist ein Schaltjahr!");
    }else{
        printf("Das Jahr ist kein Schaltjahr!");
    }

}


void CDate::show(int type){
    char* Med[12]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
    char* Ful[12]={"January","February","March","April","May","June","Juli","August","September","October","November","December"};
    if(type==SHORT){
         cout<<setw(2)<<setfill('0')<<m_day<<"."<<setw(2)<<setfill('0')<<m_month<<"."<<m_year;
     return ;
    }
        if(type==MEDIUM){
         cout<<setw(2)<<setfill('0')<<m_day<<"."<<Med[m_month]<<"."<<m_year;
        return ;
    }
        if(type==FULL){
        cout<<setw(2)<<setfill('0')<<m_day<<"."<<Ful[m_month]<<"."<<m_year;
        return ;
    }

}

ostream& operator<<(ostream& o,const CDate x){
     cout<<setw(2)<<setfill('0')<<x.m_day<<"."<<setw(2)<<setfill('0')<<x.m_month<<"."<<x.m_year;
    return o;
}






const string CDate::toString(){
    string DATE;
    DATE+= utility::toString(m_year);
    DATE+='-';
    DATE+=utility::toString(m_month);
    DATE+='-';
    DATE+=utility::toString(m_day);
    return DATE;
}
